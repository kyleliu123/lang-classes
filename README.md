# Language ID Translations
* * *
# Table of Contents
1. [Commands](#markdown-header-commands)

	  **One Time Uses**

	1. [generate-ids](#markdown-header-generate-ids)
	2. [generate-json](#markdown-header-generate-json)
	3. [convert-yaml](#markdown-header-convert-yaml)
	4. [lookup-yaml](#markdown-header-lookup-yaml)

	  **Reuse for future translation updates**

	1. [update-ids](#markdown-header-update-ids)
	2. [update-json](#markdown-header-update-json)
	3. [update-google-excel](#markdown-header-update-google-excel)
	4. [generate-excel](#markdown-header-generate-excel)
	5. [update-excel](#markdown-header-update-excel)
	6. [check-duplicate-misplaced-ids](#markdown-header-check-duplicate-misplaced-ids)

2. [Developers Best Practices](#markdown-header-developers-best-practices)
	1. [Best Practice: HTML Files](#markdown-header-best-practice-html-files)
	2. [Best Practice: JS Files](#markdown-header-best-practice-js-files)
3. [Language Translation Process](#markdown-header-language-translation-process)
4. [Package Dependencies](#markdown-header-package-dependencies)

## Commands
* * *
The following are accepted commands

**One Time Uses**

The following commands are intended to only be used once to have files follow the new pattern of translations

* [generate-ids](#markdown-header-generate-ids) - generates ids for modules with no prior ids
* [generate-json](#markdown-header-generate-json) - creates the new format of json files
* [convert-yaml](#markdown-header-convert-yaml) - converts YAML files to JSON files
* [lookup-yaml](#markdown-header-lookup-yaml) - transfers translations in YAML file to JSON file if no translation in JSON file

**Reuse for future translation updates**

The following commands are intended to be used for every translation process

* [update-ids](#markdown-header-update-ids) - adds id if they are missing on tags
* [update-json](#markdown-header-update-json) - updates json files if html strings, with existing ids, have changed. Also increments 'enVersion'
* [update-google-excel](#markdown-header-update-google-excel) - updates json files with google translate on any missing translations, will not update existing translation
* [generate-excel](#markdown-header-generate-excel) - generates excel file for translator, can specify module if needed
* [update-excel](#markdown-header-update-excel) - updates json files with new translations, also increments 'transVersion'
* [check-duplicate-misplaced-ids](#markdown-header-check-duplicate-misplaced-ids) - more a helper tool to detect duplicate ids (only lists duplicate ids, does not change files)

### generate-ids

**Command:**
```bash
$ node index.js generate-ids
```
**Desc:**

* Command is intended for completely new modules being translated for the first time, meaning the HTML files do not have IDs. 
* Will override any ids present and reindex with new order
* Command will identify tags with strings/text in them and assign them an id. This id format can be changed in the variable `moduleCode`

**`lang-id` Format:**
```html
<h1 lang="en" lang-id="CL630">Introduction</h1>
```

**`moduleCode` Defined Object:**
```js
const moduleCode = {
	'account': 'AC',
	'aerosol': 'AE',
	'airtemp': 'AI',
	'airtempminmax': 'AM',
	'atmosphere': 'AT',
	'barometricpressure': 'BA',
	'clouds': 'CL',
	'eclipse': 'EC',
	'eclipse2017': 'EL',
	'eclipse2019': 'EI',
	'help': 'HE',
	'humidity': 'HU',
	'landcover': 'LC',
	'lib': 'LB',
	'mosquitoes': 'MO',
	'observations': 'OB',
	'observer': 'OS',
	'observerpro': 'OP',
	'precipitation': 'PR',
	'rainfall': 'RA',
	'sitelocation': 'SI',
	'submissions': 'SU',
	'surfacetemperature': 'ST',
	'trees': 'TR',
	'watervapor': 'WA',
	'web': 'WE',
	'wind': 'WI'
};
```

### generate-json

**Command:**
```bash
$ node index.js generate-json
```
**Desc:**

* Will add ids to json files and match according translation from old style lang.json files. 
* Will add strings from js files to json files separated by the respective module.

**Example JSON Structure:**
```json
{
    "token": {
        "CL1": {
            "trans": [
                "What does your sky look like?",
                "¿Cómo se ve el cielo?"
            ],
            "enVersion": "1",
            "transVersion": "1"
        },
        "LC640": {
            "trans": [
                "Your analysis of this picture estimated:",
                "Su análisis de esta imagen estima:"
            ],
            "enVersion": "1",
            "transVersion": "1"
        },
        "clouds": {
            "Please enter a valid time range.": "Introduzca un intervalo de tiempo válido."
        },
        "landcover": {
            "Install Module": "instalar Módulo"
        }
    },
    "regex": []
}
```

### convert-yaml

**Command:**
```bash
$ node index.js convert-yaml
```
**Desc:**

* Converts YAML files to JSON files
* Flattens YAML file into one level JSON object
* Places converted file into the `'./yaml-json/json` directory
* `flatten()` method created to flatten nested objects and arrays within JSON

### lookup-yaml

**Command:**
```bash
$ node index.js lookup-yaml
```
**Desc:**

* Looks up JSON en strings in YAML files for translation
* Only transfers YAML translation if JSON translation is blank, can be changed but idea is to avoid overriding translator translations in JSON files
* Changes `transVersion` number to match `enVersion` number


### update-ids

**Command:**
```bash
$ node index.js update-ids
```
**Desc:**

* Will add ids to any new tags that need lang-ids
* Any new html tags will be given an id that is incremented by the last id number in module

### update-json

**Command:**
```bash
$ node index.js update-json
```
**Desc:**

* Will update existing lang json files with matched lang-ids and update the english string
* Used for when text is updated in the app so the lang json english strings match the `lang-id`
* Increments `enVersion` in JSON file if English string doesnt match its id
* Updates any missing/new `lang-id`s, may not put ids in correct order but will be in JSON file
* Removes any unused `lang-id`s from JSON files
* Uses `updatedDir = './updated'` for html files and uses `dir = './current'` for js files. Html files are used from `/updated` directory since `update-ids` command places new files in `/updated` directory

### update-google-excel

**Command:**
```bash
$ node index.js update-google-excel ./file/path
```
**Desc:**

* User must create a excel sheet with google translated values filled in. Also must name worksheet as 'languages'
* Updates json files with google translated excel file, only updates values with no translation
* `transVersion` number will not be increased, this is to flag highlight coloring when generating excel files, `generate-excel`


### generate-excel

**Command:**
```bash
$ node index.js generate-excel
```
**Optional Commands:**
```
$ node index.js generate-excel clouds full
$ node index.js generate-excel trees diff full
```
**Desc:**

* Will generate an excel file with all translations we have. 
* English phrases will be stripped of any HTML tags to prevent translation issues in Google Translate
* Optional arguments include `(module name)`, `full`, and `diff`
* Optional argument `(module name)` can be provided to divide by module. When generating excel files, duplicates are removed for translator by default. 
* Optional argument `full` can specify for duplicates to appear in excel file with argument
* Optional argument `diff` can be provided to generate excel files for only mismatched `enVersion` and `transVersion` numbers. Thought is that if mismatched, translation is incorrect and needs to be translated

### update-excel
**Command:**
```bash
$ node index.js update-excel ./file/path
```
**Desc:**

* Will update existing lang json files with excel file provided
* Second argument should be the excel file path wanted to be used for update
* Function will look for worksheet name in file titled `languages` in order to update JSON file
* Increments `transVersion` if translation string is updated

### check-duplicate-misplaced-ids
**Command:**
```bash
$ node index.js check-duplicate-misplaced-ids
```
**Desc:**

* Lists modules and their duplicate ids
* Lists modules and their misplaced ids
* Duplicate ids are not supported with this process as user can easily have multiple ids and accidentally change text that no longer matches id

## Developers Best Practices
* * *
This section is to describe the best practices when writing text for HTML files and JS files to best accomodate the language translation process and translator

### Best Practice: HTML Files

To best accomodate for translators, we want to try to minimize the amount of code (HTML, CSS, JS) that is in the English string that needs to be translated. 

#### Rules
1. All text should be wrapped in its own tag
2. Avoid nesting HTML tags within text
3. Use the `no-trans` attribute in tags if translation is not needed
4. When changing large amounts of text and HTML tags, try to best match ids

#### Common Issues
##### HTML Tags nested within text

All text should be wrapped in its own tag. We want to avoid nesting HTML tags within texts. Having our tags formatted in the fix portion, ensures that the `a` tag with the `onclick` attribute will not be part of the string that needs to be translated. This can be especially difficult for translators who are not aware of how HTML looks. Note that the word "HERE" will now be translated separately and the ending period is not translated due to the `no-trans` attribute.

**Issue:**
```html
<span lang="en">Users can visualize the observations you submit to GLOBE, as well as those submitted by other app users from around the world, by clicking <a href="#" onclick="utilities.openWindow('https://www.globe.gov/globe-data/visualize-and-retrieve-data', '_blank', 'location=no,enableViewportScale=yes');">HERE</a>.</span>
```
**Fix:**
```html
<span>
	<span lang="en">Users can visualize the observations you submit to GLOBE, as well as those submitted by other app users from around the world, by clicking </span>
	<a href="#" onclick="utilities.openWindow('https://www.globe.gov/globe-data/visualize-and-retrieve-data', '_blank', 'location=no,enableViewportScale=yes');" lang="en">HERE</a>
	<span no-trans>.</span>
</span>
```

##### When changing large amounts of text and HTML tags, try to best match ids

In situations where there is significant changes to the HTML file, the developer should best try to match the `lang-id` to the new tags/text. This is to hopefully use translations that still make sense while we wait for the translator to make updates to translations.

**Issue:**
```html
<span lang-id="CL1">Text 1</span>
<span lang-id="CL2">Text 2</span>
<span lang-id="CL3">Text 3</span>

<span lang-id="CL4">Other Text</span>
```
**Fix:**
```html
<span lang-id="CL4">Other Text</span>
<span lang-id="CL3">3 Text</span>
<span lang-id="CL2">Text 2</span>

<span lang-id="CL1">Text One</span>
```

##### Do not indent and break lines for text within tag

When capturing the strings in the tags, indents, spaces, and line breaks are accounted for to eliminate issues with strings beginning or ending with spaces. To avoid issues of unwanted characters for translation such as `\t` or `\n` avoid doing this:

**Issue:**
```html
<div ng-show="showError">
	{{subError}}
</div>
```
**Fix:**
```html
<div ng-show="showError">{{subError}}</div>
```

##### Do not have excess wrapping `lang="en"` tags
The in app translation module uses `lang="en"` to find where to translate, subsequently it assumes everything within that tag should be translated. The issue with that though is that it doesnt read individual child tags within the parent one and doesn't translate it. Simply remove the parent `land="en"`. Only tags with a `lang-id` should really only have `lang="en"`

**Issue:**
```html
<div class="selection" lang="en" ng-click="identifySiphonPecten('no, i dont see pecten')">
	<span class="text" lang="en" lang-id="MO362">No pecten (teeth) on siphon</span>
	<div class="dkRightarrow"></div>
</div>
```
**Fix:**
```html
<div class="selection" ng-click="identifySiphonPecten('no, i dont see pecten')">
	<span class="text" lang="en" lang-id="MO362">No pecten (teeth) on siphon</span>
	<div class="dkRightarrow"></div>
</div>
```


### Best Practice: JS Files
#### Rules
1. Text that needs to be translated should be wrapped in `window.lang.translate()`
2. Ensure the text translated isn't used in JS logic

#### Common Issues
##### Ensure the text translated isn't used in JS logic

Make sure that the text wrapped in the `window.lang.translate()` isn't used somewhere in the JS such as an if statement. Since what is inside the `window.lang.translate()` is translated, this replaces the value of the variable and may affect your JS code or Angular code in the HTML files. The issue below would have had the word "Hello" translated and the `if` statement would no longer check correctly.

**Issue:**
```js
var sample = window.lang.translate('Hello');

if(sample === 'Hello') {...}
```
**Fix:**
```js
var sample = 'Hello'
var sampleText = window.lang.translate('Hello');

if(sample === 'Hello') {...}
```

## Language Translation Process
* * *
The `/current` directory will serve as the directory where you will copy and paste the modules from app. Step 2 says to do the same for `/updated` but that is only for folder structure. The files within `/updated` will be updated with new `land-id`, json files, and any other changes needed when updating translations. All changes to file will be in the `/updated` directory and will be up to the user to place them in the app code

### Setup
1. Create directories `/current` and `/updated` with `/modules` folder. `/excelfiles` directory will be created automatically
2. Copy and paste all current modules inside both directories
3. Run `generate-ids` if no ids have been created for any of html files
4. Run `generate-json` to update the new format of json files accompanied by the ids created in step 3
5. Run `lookup-yaml` if needed to update JSON files with missing translations

**Sample File Structure:**
```bash
/language-module-tags
├── /current
│   └── /modules
│       ├── /clouds
│       ├── /landcover
│       ├── /trees
│       └── ...
├── /excelfiles
└── /updated
    └── /modules
        ├── /clouds
        ├── /landcover
        ├── /trees
        └── ...
```

### Run Commands
The intention is that every time something is needed for translation. The same repeated process should apply and no manual work should be needed other than running commands. If setup section above has not been done, the following commands will not work correctly.

1. Run `update-ids`. This should update any ids in HTML files and also find new tags, then add `lang-id` to them.
2. Run `update-json`. This should update the existing lang json files with any changes to English text according based on the id. Will also add any missing/new `lang-id`s
3. Run `generate-excel`. This should generate excel files in the directory `/excelfiles`. There will be a combined file with all translations in `languages.xlsx` and specific language files title by language code
4. Take phrases from the excel files and run them through Google Translate. (There is a Google Translate Sheet created on Google Sheets)
5. Run `update-google-excel` to update the json files for strings without any translations
6. Run `generate-excel` with the `diff` flag. Updates the excel files and only includes the Google translated strings. Logic is that the translator only need to change the Google translated strings.
7. Translation file will be sent out to translator. May take several weeks depending on availability. Depending on how much time has passed or how many changes have been made. Steps 1 and 2 may have to be run again to ensure updated ids and json files.
8. Once translated excel file is received, run `update-excel /filelocation` with file location of translated excel file. Also ensure the worksheet name is called `languages`


## Package Dependencies
* * *
Language translation process relies heavily on usage of the following npm packages:

* fs
	* used for writing and reading files in file system
* cheerio
	* used to parse through html files and identify texted needed for translation
	* used to assign id to html files
* html-entities
	* used to decode html entities, mainly when special characters are used
* XLSX
	* used for creation of excel files and reading of excel files

## Todo List
* * *
* Functionality to take current lang files and remove version numbers from files so it can reduce file size on production
* Create some functionality that allows for possible change 1 spot in HTML, changes all text that is the same
* Intergrate the js files with `lang-id`s as well, currently no tracking