const fs = require('fs');
const cheerio = require('cheerio');
const html5Lint = require('html5-lint');
const Entities = require('html-entities').AllHtmlEntities;
const XLSX = require('XLSX');
const entities = new Entities();
const dir = './current'; // Directory for modules, searches for all html files within directory
const updatedDir = './updated';
const langDir = './current/modules/lang'; // Directory for lang json files
const updatedLangDir = './updated/modules/lang'; // Directory for lang json files
const yamlDir = './yaml-json';
const excelDir = './excelfiles'; 
// if excelDir doesnt exist create dir
if (!fs.existsSync(excelDir)){
    fs.mkdirSync(excelDir);
}

let htmlFiles = filterFiles(getFiles(dir), 'html');

let jsonFiles = filterFiles(getFiles(langDir), 'json');
let updatedJsonFiles = filterFiles(getFiles(updatedLangDir), 'json');
var attributeLog = 'Possible HTML Tags that need lang id: ' + '\n';
var htmlLog = 'Possible HTML Errors: ' + '\n';

let defaultLang = 'en';
// define the supported languages
let supportedLanguages = ['es', 'fr', 'de', 'ru', 'ar', 'nl', 'th', 'hi', 'pt', 'vi', 'ko', 'pl', 'hr', 'lv'];

// run node index.js generate-ids
// generate-ids will populate files with no ids starting counts at 1, only use if there are no existing ids populated in html files
// update-ids will populate files with existing ids to preserve current ids with translations
// dir - specifies where the html files are, usually modules is put in here
// htmlFiles - retrieves all html files in dir
// general structure of files:
// { 'clouds': [
// 		'file1',
// 		'file2'
// 	]
// }

// update-ids will add ids to any new tags that need lang-ids

// attribute tag that defines an element to not translate:
// no-trans

// generate-json will add ids to json files and match according translation from old style lang.json files. Also will add strings from js files to json files

// generate-excel will generate an excel file with all translations we have. Optional second argument can be provided to divide by module
// when generating excel files, duplicates removed for translator. Can specify for duplicates to appear with argument 'full'
// argument 'diff' will generate excel with only diffs in version numbers and no duplicates

// update-json - will update existing lang json files with matched lang-ids and update the english string
// used for when text is updated in the app so the lang json english strings match the lang-id
// also updates any missing/new lang-ids, may not put ids in correct order

// update-excel - will update existing lang json files with excel file provided


// one time uses
// generate-ids
// generate-json
// convert-yaml

// reuse for future translation updates
// 1. update-ids - adds id if they are missing on tags
// 2. update-json - updates json files if html strings, with existing ids, have changed. Also increments 'enVersion'
// 3. generate-excel - generates excel file for translator, can specify module if needed
// 4. update-excel - updates json files with new translations, also increments 'transVersion'

// grab any arguments after the first two
let args = getCommandLineArguments();

// get the action, otherwise use help
let action = (args.length > 0) ? args[0].toLowerCase() : 'help';
let action2 = (args.length > 1) ? args[1].toLowerCase() : '';
let action3 = (args.length > 2) ? args[2].toLowerCase() : '';

let transId = 0;
let lastId = 0;
let existingIds = false;
const moduleCode = {
	'account': 'AC',
	'aerosol': 'AE',
	'airtemp': 'AI',
	'airtempminmax': 'AM',
	'atmosphere': 'AT',
	'barometricpressure': 'BA',
	'clouds': 'CL',
	'eclipse': 'EC',
	'eclipse2017': 'EL',
	'eclipse2019': 'EI',
	'help': 'HE',
	'humidity': 'HU',
	'landcover': 'LC',
	'lib': 'LB',
	'mosquitoes': 'MO',
	'observations': 'OB',
	'observer': 'OS',
	'observerpro': 'OP',
	'precipitation': 'PR',
	'rainfall': 'RA',
	'sitelocation': 'SI',
	'submissions': 'SU',
	'surfacetemperature': 'ST',
	'trees': 'TR',
	'watervapor': 'WA',
	'web': 'WE',
	'wind': 'WI'
};
const modules = Object.keys(moduleCode);
// handle the different actions
switch (action) {
	case 'generate-ids':
		generateIds();
		break;
	case 'generate-json':
		generateJson();
		break;
	case 'convert-yaml':
		convertYaml();
		break;
	case 'lookup-yaml':
		lookupYaml();
		break;
	case 'update-ids':
		updateIds();
		break;
	case 'update-json':
		updateJson();
		break;
	case 'update-google-excel':
		updateGoogleExcel(action2);
		break;
	case 'generate-excel':
		generateExcel(args.slice(1));
		break;
	case 'update-excel':
		updateExcel(action2);
		break;
	case 'check-duplicate-misplaced-ids':
		checkDuplicateMisplacedIds();
		break;
	default:
		// doHelp();
		console.log('no argument defined');
}

// returns all command line arguments after the first two
function getCommandLineArguments() {
	return process.argv.slice(2);
}

function generateIds() {
	console.log('Loading.....')
	
	// Loops through all html files in directory and calls processFiles
	for(module in htmlFiles) {
		// transId and attributeLog reset per module
		transId = 0;
		attributeLog = 'Possible HTML Tags that need lang id: ' + '\n';
		htmlFiles[module].forEach(function(htmlFile, idx) {
			let data = fs.readFileSync(htmlFile).toString();
			$ = cheerio.load(data, {decodeEntities: true});

			processFiles(htmlFile, idx, module);
		});
	}
}

function updateIds() {
	existingIds = true;
	// give new tags latest ids
	for(module in htmlFiles) {
		// transId, lastId and attributeLog reset per module
		transId = 0;
		lastId = latestId(htmlFiles, module); // get last id. Latest id(largest id), any element with new text will be given a new id after
		attributeLog = 'Possible HTML Tags that need lang id: ' + '\n';
		// loop through each file in module
		htmlFiles[module].forEach(function(htmlFile, idx) {
			let data = fs.readFileSync(htmlFile).toString();
			$ = cheerio.load(data, {decodeEntities: true});

			processFiles(htmlFile, idx, module);
		});
	}
}

function jsFilesToObj(directory) {
	// building object for en js strings
	let jsJson = {};
	let files = filterFiles(getFiles(directory), 'js');
	for(let module in files) {
		jsJson[module] = {};
		files[module].forEach(function(jsFile, idx) {
			let data = fs.readFileSync(jsFile).toString();
			$ = cheerio.load(data, {decodeEntities: true});
			
			if (data.indexOf('window.lang.translate(') !== -1) {
				var d1 = String.fromCharCode(4);
				var d2 = String.fromCharCode(5);

				var data2 = data.replace(/window\.lang\.translate\('/g, d1);
				data2 = data2.replace(/'\)/g, d2);

				data2 = data2.split(d1);
				for (var i = 1; i < data2.length; i++) {
					data2[i] = data2[i].split(d2);
					// data2[i][0] - represents the en string from js files
					jsJson[module][data2[i][0]] = '';
				}
			}
		});
	}
	return jsJson;
}

function htmlFilesToObj(directory) {
	let htmlJson = {};
	let files = filterFiles(getFiles(directory), 'html');
	for(let module in files) {
		// transId reset per module
		transId = 0;
		files[module].forEach(function(htmlFile, idx) {
			let data = fs.readFileSync(htmlFile).toString();
			$ = cheerio.load(data, {decodeEntities: true});

			// loop through all lang=id tags
			let elements = $('[lang-id]');
			// check text against json file
			for(let i = 0; i < elements.length; i++) {
				// get current id ex. "CL48"
				let currentId = $(elements[i]).attr('lang-id');
				let currentText = entities.decode($(elements[i]).html());

				// checks if id is in object, really shouldnt have any repeated ids
				if(!htmlJson[currentId]) {
					htmlJson[currentId] = {};
					htmlJson[currentId]['trans'] = [currentText, ''];
					htmlJson[currentId]['enVersion'] = '1';
					htmlJson[currentId]['transVersion'] = '0';
				} else {
					console.error('SHOULD NOT HAVE REPEATED ID:', currentId);
				}
			}
		});
	}
	return htmlJson;
}

function generateJson() {
	// building object for en js strings
	let jsJsonOriginal = jsFilesToObj(dir);

	// building object for en html strings
	let htmlJsonOriginal = htmlFilesToObj(updatedDir);

	// combining the html and js objects and retrieving their translations from existing lang files
	jsonFiles.lang.forEach(function(langFile) {
		// doesnt matter which file, all JSON files should have the same english
	  let jsonData = {};
		fs.readFile(langFile, function (error, content) {
			// create new copies of js and html strings
			let jsJson = JSON.parse(JSON.stringify(jsJsonOriginal));
			let htmlJson = JSON.parse(JSON.stringify(htmlJsonOriginal));

			// console.log(langFile)
			jsonData = JSON.parse(content).token;
			for(let string in jsonData) {
				for(let module in jsJson) {
					if(jsJson[module].hasOwnProperty(string)) {
						jsJson[module][string] = jsonData[string];
					}
				}
			}

			for(let id in htmlJson) {
				if(jsonData.hasOwnProperty(htmlJson[id]['trans'][0])) {
					htmlJson[id]['trans'][1] = jsonData[htmlJson[id]['trans'][0]];
					htmlJson[id]['transVersion'] = '1';
				}
			}

			let finalJson = {
				token: {},
		  		regex: []
			};
			finalJson['token'] = Object.assign({}, htmlJson, jsJson);


			let filepath = langFile.split('modules');
			// create new json file with id assigned to english and translated text
			fs.writeFile('./updated/modules' + filepath[1], JSON.stringify(finalJson, null, 4), function(err) {
		    if(err) {
		        return console.log(err);
		    }
			});
		});
	});
}

function updateJson() {
	// building object for en js strings
	let jsJsonOriginal = jsFilesToObj(dir);

	// building object for en html strings
	let htmlJsonOriginal = htmlFilesToObj(updatedDir);

	// replaces js, updates html strings and increments version numbers
	jsonFiles.lang.forEach(function(langFile) {
		// create new copies of js and html strings
		let jsJson = JSON.parse(JSON.stringify(jsJsonOriginal));
		let htmlJson = JSON.parse(JSON.stringify(htmlJsonOriginal));
		// doesnt matter which file, all JSON files should have the same english
  		let jsonData = {};
		fs.readFile(langFile, function (error, content) {
			jsonData = JSON.parse(content).token;
			// just replace the js portion, no id or version tracking
			// will take what is in js files and if json files have translations, will replace, if not blank
			for(let module of modules) {
				for(let string in jsJson[module]) {
					if(jsonData[module].hasOwnProperty(string)) {
						jsJson[module][string] = jsonData[module][string];
					}
				}
			}

			// compares html en strings to what is in lang directory
			for(let id in htmlJson) {
				// if en strings dont match, that means there was an update
				if(jsonData.hasOwnProperty(id) && htmlJson[id]['trans'][0] !== jsonData[id]['trans'][0]) {
					// increment enVersion by 1
					htmlJson[id]['enVersion'] = JSON.stringify(parseInt(jsonData[id]['enVersion']) + 1);
					// transfer over translation and transVersion
					htmlJson[id]['trans'][1] = jsonData[id]['trans'][1];
					htmlJson[id]['transVersion'] = jsonData[id]['transVersion'];
				} else if(jsonData.hasOwnProperty(id) && htmlJson[id]['trans'][0] === jsonData[id]['trans'][0]) {
					// if en strings do match, no update
					// transfer over translation and transVersion
					htmlJson[id]['trans'][1] = jsonData[id]['trans'][1];
					htmlJson[id]['transVersion'] = jsonData[id]['transVersion'];
				} else if(!jsonData[id]) {
					// doesn't exist, new id, add into object
					jsonData[id] = {
						"trans": [
			                htmlJson[id]['trans'][0],
			                ""
			            ],
			            "enVersion": "1",
			            "transVersion": "0"
					}
				}
			}

			// compares lang json keys strings to what is in html en strings, deletes unused keys
			for(let id in jsonData) {
				// if part of js data translation skip for now
				if(moduleCode[id]) {
					continue;
				}
				// if no key, delete the value in the jsonData
				if(!htmlJson.hasOwnProperty(id)) {
					delete jsonData[id];
				}
			}

			let finalJson = {
				token: {},
		  		regex: []
			};
			Object.assign(finalJson['token'], htmlJson, jsJson);

			let filepath = langFile.split('modules');
			// create new json file with id assigned to english and translated text
			fs.writeFile('./updated/modules' + filepath[1], JSON.stringify(finalJson, null, 4), function(err) {
			    if(err) {
			        return console.log(err);
			    }
			});
		});
	});
}

// recursively returns an array of files in a directory
function getFiles(dir, files_) {
	// start a files array or use the one provided
    files_ = files_ || [];

    // synchronously get the conents of the current directory
    let files = fs.readdirSync(dir);

    // loop through all the files
    for (let i in files) {
    	// build the name of the current file
        let name = dir + '/' + files[i];

        // if its a directory, then recursively call
        if (fs.statSync(name).isDirectory()) {
        	// pass the current name and array so far of files
            getFiles(name, files_);
        } else {
        	// it's a file not a directory, so add it to the list
            files_.push(name);
        }
    }
    // return the array of files
    return files_;
}

// Collects all specified file names
function filterFiles(filesArr, fileExtension) {
	const filteredFiles = {};

	filesArr.forEach(function(file) {
		// skip version.json file
		if(file.includes('version.json')) return;

		let fileType = file.toString().split('.')[file.toString().split('.').length - 1];

		// looks only for .html files
		if(fileType === fileExtension) {
			// grabs module name
			if(!filteredFiles[file.split('/')[3]]) {
				filteredFiles[file.split('/')[3]] = [];
			}
			filteredFiles[file.split('/')[3]].push(file);
		}
	});
	return filteredFiles;
}

// Gets deepest elements
function getDeepest(parent, result = []) {
	let children = $(parent).children();

	// do not inlcude no-trans tagged items and its children
	if($(parent).attr('no-trans') !== "") {
		if(children.length > 0){
			// Checks if element has text
			if($(parent).clone().children().remove().end().text().replace(/(?:\r\n|\r|\n)/g, '').trim() !== '') {
				result.push($(parent));
			} else {
				children.each(function(i, elem) {
					return getDeepest(elem, result);
				});
			}
		} else {
			if($(parent).text() !== '') {
				result.push($(parent));
			}
		}
	}
	return result;
}

// Returns array of deepest elements
function deepestElements() {
	let elements = [];
	// Creates array of deepest elements
	$('html').each(function(i, elem) {
		let elems = getDeepest($(elem));
		if(elems.length > 1) {
			for(let j = 0; j < elems.length; j++) {
				elements.push($(elems[j]));
			}
		} else {
			elements.push($(elems[0]));
		}
	});
	return elements;
}

// gets the latest(largest) id for the module
function latestId(htmlFiles, module) {
	lastId = 0;
	htmlFiles[module].forEach(function(htmlFile, idx) {
		let data = fs.readFileSync(htmlFile).toString();
		$ = cheerio.load(data, {decodeEntities: true});
		// Creates array of deepest elements
		let elements = deepestElements();

		for(var i = 0; i < elements.length; i++) {
			// get current id ex. "CL48"
			let currentId = $(elements[i]).attr('lang-id') ? $(elements[i]).attr('lang-id') : '';
			// parseInt of undefined will result in NaN, comparing max with NaN will result as NaN so 0 is passed as default case
			let currentIdNumber = parseInt(currentId.substring(2, currentId.length)) ? parseInt(currentId.substring(2, currentId.length)) : 0;
			lastId = Math.max(currentIdNumber, lastId);
		}
	});
	
	return lastId;
}

function addId(existingIds, currentElement, module) {
	let elementText = currentElement.text();
	// checks if elementText is angular only, do not add id
	// doesnt check for instances where text begins and ends with angular content. fail ex: {{angularContent}} hello world {{angularContent}}
	let angularOnly = elementText.substring(0, 2) === '{{' && elementText.substring(elementText.length - 2, elementText.length) === '}}';
	// regex for letters
	let regExp = /[a-zA-Z]/g;
	// uncomment for logs
	// Logs element that needs lang=id attribute
	// if(!currentElement.attr().lang) {
	// 	tempLog += 'Element: ' + currentElement.toString() + '\n';
	// }

	// Do not include in translation: empty strings, br tags, doesnt contain any letters, or angular only
	if(elementText.trim() !== '' && currentElement.get(0).tagName !== 'br' && !angularOnly && regExp.test(elementText)) {
		// no existing ids
		if(!existingIds) {
			// creates the module translation id
			transId += 1;
			let attr = moduleCode[module] + transId;

			// Add the lang=en attribute if needed
			if(!currentElement.attr('lang')) currentElement.attr('lang', 'en');
			// Add the lang-id=id attribute
			currentElement.attr('lang-id', attr);
		} else if(existingIds && !currentElement.attr('lang-id')) {
			// existing ids
			lastId += 1;
			let attr = moduleCode[module] + lastId;

			// Add the lang=en attribute if needed
			if(!currentElement.attr('lang')) currentElement.attr('lang', 'en');
			// Add the lang-id=id attribute
			currentElement.attr('lang-id', attr);
		}
	}
}

// Finds elements with text within file and places lang="en" attributes
function processFiles(fileName, idx, module) {
	// Creates array of deepest elements
	let elements = deepestElements();

	// Gives elements lang="en" attribute, excluding tags without text and <br> tags
	var tempLog = '';
	for(var k = 0; k < elements.length; k++) {
		let currentElement = $(elements[k]);
		addId(existingIds, currentElement, module);
	}
	tempLog.length > 0 ? attributeLog += 'File Name: ' + fileName + '\n' + tempLog : '';

	// Line break for readability
	if(attributeLog.includes('Element: ')) {
		tempLog.length > 0 ? attributeLog += '\n' : '';
	}

	if(idx === htmlFiles[module].length - 1) {
		console.log('Module:', module);
		console.log(attributeLog);
	}

	if(elements.length > 0) {
		writeLanguageFile(fileName);
	}
}

// write file with the lang attribute
function writeLanguageFile(fileName) {
	let cleanHTML = entities.decode($.html().toString());

	// Remove <head> and <body> tags
	const replaceTags = ['<html>', '</html>', '<head>', '</head>', '<body>', '</body>'];
	replaceTags.forEach(function(tag) {
		cleanHTML = cleanHTML.replace(tag, '');
	})

	let filepath = fileName.split('modules');

	// Creates new HTML file
	fs.writeFile('./updated/modules' + filepath[1], cleanHTML, function(err) {
	// If overwrite of file is wanted
	// fs.writeFile(fileName, cleanHTML, function(err) {
	    if(err) {
	        return console.log(err);
	    }
	});
}


function generateExcel(genExcelArgs) {
	// argument and type definitions for type of excel file generation
	let module = '';
	let diff = false;
	let full = false;
	for (let i = 0; i < genExcelArgs.length; i++) {
		if(genExcelArgs[i] === 'diff') {
			diff = true;
		} else if(genExcelArgs[i] === 'full') {
			full = true;
		} else {
			module = genExcelArgs[i];
		}
	}
	console.log('generate excel action called', module);

	// load the existing language packs
	// var langPacks = loadLangPacks(langDir);
	var langPacks = loadLangPacks(updatedLangDir);

	// convert the langPacks to an array
	var overallData = langPacksToArray(langPacks, module, diff);

	// removes duplicates on default, specify full for all ids with duplicates in excel file
	if(!full) {
		overallData = removeDuplicateData(overallData);
	}

	for (let i = 0; i < overallData.length; i++) {
		// start a workbook
		var wb = new Workbook();

		// define a worksheet name
		// var wsName = i === 0 ? 'languages' : overallData[i][0][2];
		var wsName = 'languages';

		// convert the data array to a worksheet object
		var ws = arrayToWorksheet(overallData[i]);

		// add to the workbook
		wb.SheetNames.push(wsName);
		wb.Sheets[wsName] = ws;

		let fileName = (i === 0 ? 'languages' : overallData[i][0][2]) + (module ? '_' + module : '');
		// write it
		XLSX.writeFile(wb, './excelfiles/' + fileName + '.xlsx');
	}

	return;
}

function updateGoogleExcel(updateFileURL) {
	updateExcel(updateFileURL, true);
}

function updateExcel(updateFileURL, fromGoogleTrans) {
	console.log('update action called');

	// grab the update file from the args
	var updateFile = updateFileURL;

	// load the existing language packs
	var langPacks = fromGoogleTrans ? loadLangPacks(updatedLangDir) : loadLangPacks(langDir);

	// read the excel file into an array
	var wb = XLSX.readFile(updateFile);
	// looks for sheet name as 'languages'
	var data = XLSX.utils.sheet_to_json(wb.Sheets['languages']);

	// update the lang packs with this data array
	langPacks = updateLangPacksFromExcelData(data, langPacks, fromGoogleTrans);

	// save the changes
	saveLanguagePacks(langPacks, updatedLangDir);

	return;
}

function updateLangPacksFromExcelData(data, langPacks, fromGoogleTrans) {
	// loop through all the objects in the provided array
	for (let i = 0; i < data.length; i++) {
		let key;

		// loop through our langPacks languages
		for (var lang in langPacks) {
			// this lang pack language is included in this object/row from the excel sheet
			if (typeof data[i][lang] != 'undefined') {
				if(data[i]['lang-id']) {
					// splits keys (same string, different ids)
					key = data[i]['lang-id'].split(',');

					// loop through keys, replacing translations
					for (let j = 0; j < key.length; j++) {
						if (langPacks[lang].data.token.hasOwnProperty(key[j])) {
							// update only if fromGoogleTrans && no translation value OR fromGoogleTrans is false meaning we are running updateExcel command
							if(fromGoogleTrans && !langPacks[lang].data.token[key[j]]['trans'][1] || !fromGoogleTrans) {
								// if translation is different than what is in json file, update
								if(langPacks[lang].data.token[key[j]]['trans'][1] !== data[i][lang]) {
									langPacks[lang].data.token[key[j]]['trans'][1] = data[i][lang];
									// do not increase transVersion if updating from google translate
									if (!fromGoogleTrans) {
										langPacks[lang].data.token[key[j]]['transVersion'] = (parseInt(langPacks[lang].data.token[key[j]]['transVersion']) + 1).toString();

										// if en string matches the excel version, en and trans should be at same version number
										if(langPacks[lang].data.token[key[j]]['trans'][0] === data[i]['en']) {
											langPacks[lang].data.token[key[j]]['enVersion'] = langPacks[lang].data.token[key[j]]['transVersion'];
										}
									}
								}
							}
							
						} else {
							// console.log('ID NOT FOUND\t' + key);

							// Some strings from JS could be the same as HTML id portion, have to do same check as below
							// key doesnt exist, means its part of the JS portion
							// key will be the en string
							key = data[i][defaultLang];
							// js portion update
							for (let k = 0; k < modules.length; k++) {
								let moduleData = langPacks[lang]['data']['token'][modules[k]];
								if (moduleData && moduleData.hasOwnProperty(key)) {
									moduleData[key] = data[i][lang];
								}
							}
						}
					}
				} else {
					// lang-id doesnt exist, means its part of the JS portion
					// key will be the en string
					key = data[i][defaultLang];
					// js portion update
					for (let k = 0; k < modules.length; k++) {
						let moduleData = langPacks[lang]['data']['token'][modules[k]];
						if (moduleData && moduleData.hasOwnProperty(key)) {
							moduleData[key] = data[i][lang];
						}
					}
				}
			}
		}
	}
	return langPacks;
}

function saveLanguagePacks(langPacks, langPackDirectory) {
	// loop through the languages in the langPacks object
	for (var lang in langPacks) {
		fs.writeFile(langPackDirectory + '/' + lang + '.json', JSON.stringify(langPacks[lang].data, null, 4), (err) => {
			if (err) throw err;
			console.log('The file has been saved!');
		});
	}

	return;
}


function langPacksToArray(langPacks, module, diff) {
	// start the array that will include all the lang pack data
	var data = [];
	// add an array to the data array
	var headingRow = [];
	// first column will be lang-id or if not available, blank
	headingRow.push('lang-id');
	// second column will be en
	headingRow.push(defaultLang);
	// subsequent columns will be various translated languages
	for (var lang in langPacks) {
		headingRow.push(lang);
	}
	// html tag column, displays that the is html that needs to be manually added back to translation
	headingRow.push('html');
	data.push(headingRow);

	// need to find a language that is included in these language packs
	var includedLang = supportedLanguages[0];
	for (var i = 0; i < supportedLanguages.length; i++) {
		if (langPacks.hasOwnProperty(supportedLanguages[i])) {
			includedLang = supportedLanguages[i];
			break;
		}
	}

	// loop through all the properties in the first lang pack
	for (var key in langPacks[includedLang].data.token) {
		// if module is defined, check if current id belongs to module
		if(module && key.substring(0, 2) !== moduleCode[module] && module !== key) continue;
		// if type === 'diff' only select mismatched version numbers
		if(diff && langPacks[includedLang].data.token[key]['enVersion'] && langPacks[includedLang].data.token[key]['enVersion'] === langPacks[includedLang].data.token[key]['transVersion']) continue;
		
		// start the row
		var row = [];
		
		if(langPacks[includedLang].data.token[key]['trans']) {
			// contains html? place at end of row to flag for developer to manually add back in translation
			let htmlString = '';
			if(langPacks[includedLang].data.token[key]['trans'][0].match(/(<([^>]+)>)/gi)) {
				htmlString = langPacks[includedLang].data.token[key]['trans'][0];
			}
			// push lang-id,  english string
			row.push(key, langPacks[includedLang].data.token[key]['trans'][0].replace(/(<([^>]+)>)/gi, ''));

			// loop through the supported languages
			for (var lang in langPacks) {
				var value = '';

				if(typeof langPacks[lang].data.token[key] != 'undefined') {
					if(langPacks[lang].data.token[key]['trans']) {
						// adding translated language string
						value = langPacks[lang].data.token[key]['trans'][1];

						// check and remove any html tags that are part of translation
						let strippedString = value.replace(/(<([^>]+)>)/gi, '');
						
						delete langPacks[lang].data.token[key];
						row.push(strippedString);
					}
				}
			}
			// push html string if the string contains html tags
			row.push(htmlString);
			// row complete, push row into data
			data.push(row);
		} 

		// Filling in js strings rows
		if(Object.keys(moduleCode).includes(key) && (module === key || module === '')) {
			// if module is defined or if no module is passed in arguments
			for(var string in langPacks[lang].data.token[key]) {
				row = [];
				// push empty id, english string
				row.push('', string);
				// loop through and add all translated language strings to row
				for (var lang in langPacks) {
					value = langPacks[lang].data.token[key][string];
					row.push(value);
				}
				// row complete, push row into data
				data.push(row);
			}
		}
	}
	// Overall data array
	// First item in array contains lang-id, subsequent will contain [en, various language]
	var overallData = [data];
	// supportedLanguages offset by 1 because heading in data has extra colunn of lang-id
	for (var j = 1; j <= supportedLanguages.length + 1; j++) {
		var tempArr = [];
		for (var i = 0; i < data.length; i++) {
						// lang-id  ,    en     , trans string
			tempArr.push([data[i][0], data[i][1], data[i][j]])
		}
		overallData.push(tempArr)
	}
	return overallData;

}

// builds an object of existing strings and their translations from the language packs
function loadLangPacks(langPackDirectory) {
	// start an object for the language packs
	var langPacks = {}

	// get a list of the lang pack files
	var langPackFiles = getLangPacks(langPackDirectory);

	// add the supported languages to the langPacks object
	for (var i = 0; i < supportedLanguages.length; i++) {
		// add an empty object for each language
		langPacks[supportedLanguages[i]] = {'fileName': '', 'data': getEmptyLangPackObject()};
	}

	// loop through the lang pack files
	for (var i = 0; i < langPackFiles.length; i++) {
		langPacks = loadLangPack(langPackFiles[i], langPacks);
	}

	return langPacks;
}

// this function will return an array of language packs from the language pack directory
function getLangPacks(langPackDirectory) {
	// get a list of all the files in the lang pack directory and remove any that don't match the aa.json pattern
	var langPackFiles = getFiles(langPackDirectory);
	// loop through the files and delete if it doesn't match the pattern
	for (var i = langPackFiles.length - 1; i >= 0; i--) {
		var fileName = langPackFiles[i].split('/').pop().split('.');
		if (fileName.length !== 2) {
			langPackFiles.splice(i, 1);
		} else if (fileName[0].length !== 2 || fileName[1] !== 'json') {
			langPackFiles.splice(i, 1);
		}
	}
	return langPackFiles;
}

// this function will load the file contents of the language packs and store them in the langPacks object
function loadLangPack(fileName, langPacks, lang) {
	// read the contents of the file and parse it into a local variable
	var fileData = JSON.parse(fs.readFileSync(fileName).toString());
	
	// get the language identifier from the filename
	var lang = lang ? lang : fileName.split('/').pop().split('.')[0];

	// make sure we have a token and a regex
	if (typeof fileData.token === 'undefined' || typeof fileData.regex === 'undefined') {
		// replace the filedata with the empty object
		fileData = getEmptyLangPackObject();
	}

	// now add it to the appropriate object in the lang pack
	langPacks[lang].data = fileData;

	// also save the file name
	langPacks[lang].fileName = fileName;

	return langPacks;
}

function removeDuplicateData(data) {
	for (let j = 0; j < data.length; j++) {
		// keeps track of repeated strings
		let exists = {};
		for(let i = 0; i < data[j].length; i++) {
			if(exists[data[j][i][1]]) {
				// concats ids for update use
				data[j][exists[data[j][i][1]]][0] = data[j][exists[data[j][i][1]]][0] + ',' + data[j][i][0];
				data[j].splice(i, 1);
				i -= 1;
			} else {
				// position of repeated string
				exists[data[j][i][1]] = i;
			}
		}
	}

	return data;
}

function getEmptyLangPackObject() {
	return { 'token': {}, 'regex': [] };
}

function Workbook() {
	if(!(this instanceof Workbook)) return new Workbook();
	this.SheetNames = [];
	this.Sheets = {};
}

function arrayToWorksheet(data, opts) {
	var ws = {};
	var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
	for(var R = 0; R != data.length; ++R) {
		for(var C = 0; C != data[R].length; ++C) {
			if(range.s.r > R) range.s.r = R;
			if(range.s.c > C) range.s.c = C;
			if(range.e.r < R) range.e.r = R;
			if(range.e.c < C) range.e.c = C;
			var cell = {v: data[R][C] };
			if(cell.v == null) continue;
			var cell_ref = XLSX.utils.encode_cell({c:C,r:R});
			
			if(typeof cell.v === 'number') cell.t = 'n';
			else if(typeof cell.v === 'boolean') cell.t = 'b';
			else if(cell.v instanceof Date) {
				cell.t = 'n'; cell.z = XLSX.SSF._table[14];
				cell.v = datenum(cell.v);
			}
			else cell.t = 's';
			ws[cell_ref] = cell;
		}
	}
	if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
	return ws;
}

function datenum(v, date1904) {
	if(date1904) v+=1462;
	var epoch = Date.parse(v);
	return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
}

// flattens objects with nested objects/arrays
function flatten(obj, dictionary = {}) {
	for(let key in obj) {	
		if(Array.isArray(obj[key])) {
			obj[key].forEach(function(elem, i) {
				dictionary[key + i] = elem;
			});
		} else if(typeof obj[key] === 'object') {
			flatten(obj[key], dictionary)
		} else {
			dictionary[key] = obj[key];
		}
	}
	return dictionary;
}


function convertYaml() {
	let enYaml = './yaml-json/en.json';
	let yamlFiles = filterFiles(getFiles(yamlDir), 'json')['undefined'];
	// build the english json object
	let enJson = {};
	fs.readFile(enYaml, function (error, content) {
		let jsonData = JSON.parse(content).en;
		enJson = flatten(jsonData);
	});
	// make json directory if needed
	if (!fs.existsSync('./yaml-json/json')){
    fs.mkdirSync('./yaml-json/json');
	}
	// combine en and translations
	for(let file of yamlFiles) {
		fs.readFile(file, function (error, content) {
			let filepath = file.split('/');
			let jsonData = JSON.parse(content)[filepath[2].substring(0, 2)];
			let flatData = flatten(jsonData);
			let writeData = {};
			for(let enKey in enJson) {
				if(flatData[enKey]) {
					writeData[enJson[enKey]] = flatData[enKey];
				}
			}
			
			fs.writeFile('./yaml-json/json/' + filepath[2], JSON.stringify(writeData, null, 4), function(err) {
		    if(err) {
		      return console.log(err);
		    }
			});
		});
	}
}

function lookupYaml() {
	// transfers any yaml translations into lang json files
	updatedJsonFiles.lang.forEach(function(langFile) {
	  let jsonData = {};
	  let yamlData = {};
	  let langFileName = langFile.split('/')[4];
	  let filepath = yamlDir + '/json/' + langFileName;

	  if (!fs.existsSync(yamlDir + '/json/' + langFileName)){
	    return;
		}
	  fs.readFile(yamlDir + '/json/' + langFileName, function (error, content) {
	  	yamlData = JSON.parse(content);
	  });

	  // console.log(langFile)
		fs.readFile(langFile, function (error, content) {
			jsonData = JSON.parse(content).token;

			// checks modules js portion
			for(let module of modules) {
				for(let jsonKey in jsonData[module]) {
					// checks if in yaml data and currently blank in json data
					// can change determination of whether or not we want yaml files to take precedence
					if(yamlData[jsonKey] && jsonData[module][jsonKey] === '') {
						jsonData[module][jsonKey] = yamlData[jsonKey];
					}
				}
			}

			// checks id portion
			for(let id in jsonData) {
				// check for id portion  &&     if en string is in yaml data   &&  json trans portion is blank, avoid overriding translator trans
				if(jsonData[id]['trans'] && yamlData[jsonData[id]['trans'][0]] && jsonData[id]['trans'][1] === '') {
					jsonData[id]['trans'][1] = yamlData[jsonData[id]['trans'][0]];
					jsonData[id]['transVersion'] = jsonData[id]['enVersion'];
				}
			}
			let finalJson = {
				token: {},
		  	regex: []
			};
			// to replicate json object format needed
			Object.assign(finalJson['token'], jsonData);
			fs.writeFile(updatedLangDir + '/' + langFileName, JSON.stringify(finalJson, null, 4), function(err) {
		    if(err) {
		      return console.log(err);
		    }
			});
		});
	});
}

function checkDuplicateMisplacedIds() {
	for(module in htmlFiles) {
		// tracks values for duplicate ids
		let duplicates = {};
		let duplicatesList = [];
		// tracks values for misplaced ids
		let misplacedList = [];
		htmlFiles[module].forEach(function(htmlFile, idx) {
			let data = fs.readFileSync(htmlFile).toString();
			$ = cheerio.load(data, {decodeEntities: true});
			// Creates array of deepest elements
			let elements = deepestElements();

			for(var i = 0; i < elements.length; i++) {
				// get current id ex. "CL48"
				let currentId = $(elements[i]).attr('lang-id') ? $(elements[i]).attr('lang-id') : '';
				// if no id, dont track and move on
				if (currentId === '') continue;

				// misplaced list
				if (currentId.substring(0, 2) !== moduleCode[module]) misplacedList.push(currentId);

				// duplicate list
				if (duplicates[currentId] === 0) {
					duplicates[currentId] += 1;
					duplicatesList.push(currentId);
				} else {
					duplicates[currentId] = 0;
				}
			}
		});
		if (misplacedList.length > 0) {
			console.log(`${module} has the following misplaced ids: ${misplacedList.join(', ')}`);
			// readability
			if (duplicatesList.length === 0) console.log('\n');
		}
		if (duplicatesList.length > 0) {
			console.log(`${module} has the following duplicate ids: ${duplicatesList.join(', ')}\n`);
		} 
	}
}

